# MOCK-SDP-CONSISTENZA-CLIENTI #

Questo README descrive la simulazione del progetto Back-End di SDP.

### Environment ###

* java 1.8
* SpringBoot
* Couchbase
* Maven
* Gradle

### Couchbase Environment ###

L'applicativo in questione gestisce le configurazioni del portale memorizzando su Couchbase solo le configurazioni pending sotto forma di JSON.
Il bucket utilizzato si chiama : **SDP_CLIENTI_CONSISTENZE**

I passi da eseguire per inizializzare correttamente l'ambiente Couchbase sono i seguenti:

* Creare un nuovo Bucket con il nome **SDP_CLIENTI_CONSISTENZE**;
* Creare un nuovo User con nome: **SDP_CLIENTI_CONSISTENZE** e password **Administrator**;
* Creare un indice primario lanciando la query: CREATE PRIMARY INDEX `#primary` ON `SDP_CLIENTI_CONSISTENZE`;


### RUN ###

* Creare il bucket e i vari indici su couchbase
* Impostare le variabili d'ambiente.


### RILASCIO IN COLLAUDO ###

* Basta lanciare il comando clean build

Nel caso in cui si voglia lanciare questo jar basta impostare le seguinti variabili d'ambiente:

* COUCHBASE_USERNAME=SDP_CLIENTI_CONSISTENZE
* COUCHBASE_HOST=localhost
* COUCHBASE_PASSWORD=Administrator
* COUCHBASE_PORT=8091