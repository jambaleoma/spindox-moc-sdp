package it.spindox.gate.request.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.spindox.middletier.iface.consumer.ConsumerService;
import it.spindox.middletier.service.consistenze.model.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;

@Api(value = "/", description = "Componente che Simila una POST da parte di TIBCO verso SDP")
@RestController

public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

    /**
     * Add Consumer from TIBCO
     * <p>
     * <p><b>200</b> - Added Cunsomer from TIBCO
     * <p><b>400</b> - Bad Request
     *
     * @param body The body parameter
     * @return Request from TIBCO
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    @ApiOperation(value = "Creates a 'Consumer from TIBCO'", nickname = "createConsumer", response = String.class, httpMethod = "POST")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/v1/sdp/clienti", method = RequestMethod.POST)
    public ResponseEntity<Consumer> addClienteFromTIBCO(@RequestBody Consumer body) {

        Consumer result;

        try {

            result = consumerService.save(body);

        } catch (Exception ex) {

            throw ex;
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Add Consistenza from TIBCO
     * <p>
     * <p><b>200</b> - Added Consistenza from TIBCO
     * <p><b>400</b> - Bad Request
     *
     * @param body The body parameter
     * @return Request from TIBCO
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    @ApiOperation(value = "Creates a 'Consistenza from TIBCO'", nickname = "createConsisistenza", response = String.class, httpMethod = "POST")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/consistenze", method = RequestMethod.POST)
    public ResponseEntity<Consumer> addConsistenzaFromTIBCO(@RequestBody Consumer body) {

        Consumer result;

        try {

            result = consumerService.save(body);

        } catch (Exception ex) {

            throw ex;
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Get Consumer by Id
     * <p>
     * <p><b>200</b> - Get Consumer from CB
     * <p><b>400</b> - Bad Request
     *
     * @return Consumer from CB
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    @ApiOperation(value = "Get a 'Consumer - Consistenza'", nickname = "findConsumerConsistenza", response = String.class, httpMethod = "GET")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/test/customerConsistenza/{id}", method = RequestMethod.GET)
    public ResponseEntity<Consumer> getConsumerById(@PathVariable("id") String id) {

        Consumer result;

        try {

            result = consumerService.find(id);
            result.setId(null);

        } catch (Exception ex) {

            throw ex;
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
