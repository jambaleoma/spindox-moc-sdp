package it.spindox;

import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@SpringBootApplication
@EnableSwagger2
@EnableCouchbaseRepositories
public class SDPMockApplication {

    @Value("${swagger.sdp-mock.title}")
    private String swaggerTitle;

    @Value("${swagger.sdp-mock.version}")
    private String swaggerVersion;

    @Value("${swagger.sdp-mock.title.description}")
    private String swaggerTitleDescription;

    @Value("${swagger.sdp-mock.groupname}")
    private String swaggerGroupName;


    @Autowired
    Environment environment;


    public static void main(String[] args) {
        SpringApplication.run(SDPMockApplication.class, args);
    }

    /*
     * gestione API Swagger
     */
    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(swaggerGroupName)
                .apiInfo(apiInfo())
                .select()
                .paths(Predicates.not(regex("/error.*")))
                .paths(Predicates.not(regex("/hub.*")))
                .paths(Predicates.not(regex("/actuator.*")))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerTitle)
                .description(swaggerTitleDescription)
                .version(swaggerVersion)
                .build();
    }

}
