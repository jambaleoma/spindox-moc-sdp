package it.spindox.middletier.iface.consumer;

import it.spindox.middletier.service.consistenze.model.Consumer;

public interface ConsumerService {

    Consumer save(Consumer consumer);
    Consumer find(String id);

}
