package it.spindox.middletier.service.consistenze.model;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;

@Document
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class Profilo implements Serializable {
    @Field
    private String tipoCliente;
    @Field
    private String codiceAcli;
    @Field
    private String stato;
    @Field
    private String partitaIva;
    @Field
    private String ragioneSociale;
    @Field
    private String referenteLegale;
    @Field
    private String numMaxLineeAssociabili;
    @Field
    private String marcaggio;
    @Field
    private String flagDip;
    @Field
    private String cognome;
    @Field
    private String nome;
    @Field
    private String dataNascita;
    @Field
    private String comuneNascita;
    @Field
    private String provinciaNascita;
    @Field
    private String specializzazione;
    @Field
    private String sesso;
    @Field
    private String titolo;
}
