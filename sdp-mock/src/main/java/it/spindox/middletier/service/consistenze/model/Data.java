package it.spindox.middletier.service.consistenze.model;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;

@Document
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class Data implements Serializable {
    @Field
    private String id;
    @Field
    private String rifCliente;
    @Field
    private String numLinea;
    @Field
    private String tiid;
    @Field
    private String tiidOwner;
    @Field
    private String relationKey;
    @Field
    private String sourceSystem;
    @Field
    private String marcaggio;
    @Field
    private StatoLinea statoLinea;
    @Field
    private Profilo profilo;
    @Field
    private Contatto contatto;
    @Field
    private Documento documento;
    @Field
    private Indirizzi[] indirizzi;
    @Field
    private Cliente[] cliente;
    @Field
    private Offerta[] offerta;
    @Field
    private ProdottoStandAlone[] prodottoStandAlone;
    @Field
    private String statoCertificazioneCliente;

}