package it.spindox.middletier.service.consistenze.repository;

import it.spindox.middletier.service.consistenze.model.Consumer;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

@Repository
@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "consistenza")
public interface ConsumerRepository extends CouchbaseRepository<Consumer, String> {

}

