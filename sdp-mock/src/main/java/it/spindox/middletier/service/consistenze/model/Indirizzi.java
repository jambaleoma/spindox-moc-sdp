package it.spindox.middletier.service.consistenze.model;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;

@Document
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class Indirizzi implements Serializable {
    @Field
    private String codiceToponomastico;
    @Field
    private String particellaToponomastica;
    @Field
    private String indirizzo;
    @Field
    private String civico;
    @Field
    private String cap;
    @Field
    private String siglaprovincia;
    @Field
    private String provincia;
    @Field
    private String citta;
    @Field
    private String piano;
    @Field
    private String scala;
    @Field
    private String interno;
    @Field
    private String complesso;
    @Field
    private String nazione;
    @Field
    private String tipo;
}