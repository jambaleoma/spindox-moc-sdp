package it.spindox.middletier.service.consistenze.service;

import it.spindox.middletier.iface.consumer.ConsumerService;
import it.spindox.middletier.service.consistenze.model.Consumer;
import it.spindox.middletier.service.consistenze.repository.ConsumerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConsumerServiceImpl implements ConsumerService {

    @Autowired
    private ConsumerRepository consumerRepository;

    @Override
    public Consumer save(Consumer model) {

        model.setId(model.getData().getId());

        Consumer consumer = consumerRepository.save(model);

        return consumer;
    }

    @Override
    public Consumer find(String id) {

        Optional<Consumer> consumer = null;

        if (id.length() > 0) {
            consumer = consumerRepository.findById(id);
        }

        if(consumer.isPresent()) {
            return consumer.get();
        }

        return null;
    }
}
