package it.spindox.middletier.service.consistenze.model;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;

@Document
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class Header implements Serializable {
    @Field
    private String eventId;
    @Field
    private String sourceSystem;
    @Field
    private String eventNotificationTime;
    @Field
    private String sourceSystemTime;
    @Field
    private String eventType;
    @Field
    private String entityToBeUpdated;
    @Field
    private String entityId;
    @Field
    private String ver;
}
