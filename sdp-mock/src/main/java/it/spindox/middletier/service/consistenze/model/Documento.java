package it.spindox.middletier.service.consistenze.model;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;

@Document
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class Documento implements Serializable {
    @Field
    private String tipo;
    @Field
    private String numero;
    @Field
    private String rilasciatoDa;
    @Field
    private String dataRilascio;
    @Field
    private String dataScadenza;
    @Field
    private String provinciaRilascio;
    @Field
    private String comuneRilascio;
}
