package it.spindox.middletier.service.consistenze.model;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;

@Document
@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class Offerta implements Serializable {
    @Field
    private String id;
    @Field
    private String subtype;
    @Field
    private String name;
    @Field
    private String offerId;
    @Field
    private String offerIdWeb;
    @Field
    private String ambito;
    @Field
    private String dataAttivazione;
    @Field
    private String dataCessazione;
    @Field
    private String statoCommerciale;
    @Field
    private String tipoPagamento;
    @Field
    private String metodoPagamento;
    @Field
    private String billingProfile;
    @Field
    private String idInvariante;
    @Field
    private String type;
    @Field
    private String codiceContratto;
    @Field
    private ServizioAbilitante[] servizioAbilitante;
}